const querystring = require('querystring');


function handler(response) {
  let data = '';
  response.on('data', function (chunk) {
    data += chunk;
  });
  response.on('end', function () {
    console.log(data);
  });
}

const http = require('http');
let data = '';

function rpc_request(data) {
  request = http.request({ hostname: '127.0.0.1', port: 1337, path: '/rpc', method: 'POST', headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Content-Length': Buffer.byteLength(data)  }  });
  request.write(data); request.on('response', handler); request.end();
}

// list all users   ---------------------------
rpc_request(querystring.stringify({ 'method': 'listUsers'}));

// add new user
rpc_request(querystring.stringify({ 'method': 'addUser',  'name': 'Olga', 'score': 31 }));

// remove  user
rpc_request(querystring.stringify({ 'method': 'removeUser',  'id': 1  }));

// remove  user
rpc_request(querystring.stringify({ 'method': 'updateUser',  'id': 0, 'name' : 'Andrey', score: 239  }));

// list all users   ---------------------------
rpc_request(querystring.stringify({ 'method': 'listUsers'}));
