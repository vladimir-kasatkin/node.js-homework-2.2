const querystring = require('querystring');


function handler(response) {
  let data = '';
  response.on('data', function (chunk) {
    data += chunk;
  });
  response.on('end', function () {
    console.log(data);
  });
}


const http = require('http');
let data = '';

// check initial state ---------------------------
let request = http.request({ hostname: '127.0.0.1', port: 1337, path: '/users/', method: 'GET'});
request.write(data); request.on('response', handler); request.end();

// add new user  ---------------------------
data = querystring.stringify({  'name': 'Sergey',  'score': 33 });
request = http.request({ hostname: '127.0.0.1', port: 1337, path: '/users', method: 'POST', headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Content-Length': Buffer.byteLength(data)  }  });
request.write(data); request.on('response', handler); request.end();

// show specified user  ---------------------------
request = http.request({ hostname: '127.0.0.1', port: 1337, path: '/users/2', method: 'GET'});
request.write(data); request.on('response', handler); request.end();

// show specified but not existing user  ---------------------------
request = http.request({ hostname: '127.0.0.1', port: 1337, path: '/users/101', method: 'GET'});
request.write(data); request.on('response', handler); request.end();

// remove user  ---------------------------
request = http.request({ hostname: '127.0.0.1', port: 1337, path: '/users/1', method: 'DELETE'});
request.write(data); request.on('response', handler); request.end();

// update  user  ---------------------------
data = querystring.stringify({  'name': 'Marina',  'score': 7 });
request = http.request({ hostname: '127.0.0.1', port: 1337, path: '/users/0', method: 'PUT', headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Content-Length': Buffer.byteLength(data)  }  });
request.write(data); request.on('response', handler); request.end();

// check state ---------------------------
request = http.request({ hostname: '127.0.0.1', port: 1337, path: '/users/', method: 'GET'});
request.write(data); request.on('response', handler); request.end();
