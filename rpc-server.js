const express = require("express");
const app = express();
var bodyParser = require("body-parser");

// initial state
let users = new Array(
    {'name':'Ivan', 'score':5},
    {'name':'Petr', 'score':6}
  );

// structure to string
function usersToString(arr) {
  let str = '';
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] != undefined) {
      str = str + i + ": " + arr[i].name + " : " + arr[i].score + "\n";
    }
  }
  return str;
}


let RPC = {

  'addUser': function(params, callback) {
    console.log("\n- add new user: " + params.name + ", " + params.score);
    users.push({'name': params.name, 'score': params.score});
    let id = users.length - 1;
    callback( undefined, {'id': id } );
  },

  'listUsers': function(params, callback) {
    console.log("\n- show all users");
    console.log(users);
    callback(undefined, users);
  },

  'removeUser': function(params, callback) {
    console.log("\n- remove user id = " + params.id);
    callback(undefined, {});
  },

  'updateUser': function(params, callback) {
    console.log("\n- update user id : "  + params.id + " with " + params.name + ", " + params.score);
    users[params.id] = {'name': params.name, 'score': params.score};
    callback( undefined, {} );
  },
};

var urlencodedParser = bodyParser.urlencoded({extended: false});

app.post("/rpc", urlencodedParser, function(req, res) {
  const method = RPC[req.body.method];
  method(req.body, function(error, result) {
    res.json(result);
  });
});

// обработка ошибок
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('500 Something broke!');
});

app.use(function(req, res, next){
    res.status(404);
    res.send(' 404 Not found');
    return;
});


app.listen(1337);


/*



// CRUD : show info for all users
app.get('/users', function(req, res) {
  res.send("\n- show all users:\n" + usersToString(users));
});

// CRUD : show info for specified by id user
app.get('/users/:id', function(req, res) {
  if (users[req.params.id]) {
    res.send("\n- show info for user with id = " + req.params.id + " :\n name: " + users[req.params.id].name + ", score: " + users[req.params.id].score);
  } else {
    res.send("\n- try show info for user with id = " + req.params.id + " - FAILED !");
  }
});

// CRUD : add user
app.post('/users', urlencodedParser, function(req, res) {
  users.push({'name': req.body.name, 'score': req.body.score});
  let id = users.length - 1;
  res.send("\n- added new user ("+ req.body.name + ", " + req.body.score + "), ID = " + id);
});

// CRUD : remove users
app.delete('/users', function(req, res) {
  res.send("\n- remove all users");
  users = [];
});

// CRUD : remove specified user
app.delete('/users/:id', function(req, res) {
  res.send("\n- remove users id = " + req.params.id );
  users[req.params.id] = undefined;
});

// CRUD : update specified user
app.put('/users/:id', urlencodedParser, function(req, res) {
  if (users[req.params.id]) {
    users[req.params.id] = {'name': req.body.name, 'score': req.body.score};
    res.send("\n- update info for user with id = " + req.params.id + " :\n name: " + users[req.params.id].name + ", score: " + users[req.params.id].score);
  } else {
    res.send("\n- try update info for user with id = " + req.params.id + " - FAILED !");
  }
});


// обработка 404


*/
